ALTER TABLE public.treatment_plan
  ADD CONSTRAINT treatment_plan_specialist_id_fk
FOREIGN KEY (specialist_id) REFERENCES public.specialist (id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE public.treatment_plan
  ADD CONSTRAINT treatment_plan_userinfo_id_fk
FOREIGN KEY (id) REFERENCES public.userinfo (id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE public.diagnosis
  ADD CONSTRAINT diagnosis_id_treatment_plan_id_fk
FOREIGN KEY (id) REFERENCES public.treatment_plan (id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE public.diagnosis
  ADD CONSTRAINT diagnosis_problem_id_problem_id_fk
FOREIGN KEY (problem_id) REFERENCES public.problem (id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE public.diagnosis
  ADD CONSTRAINT diagnosis_user_id_userinfo_id_fk
FOREIGN KEY (userinfo_id) REFERENCES public.userinfo (id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE public.problem
  ADD CONSTRAINT problem_example_solution_example_work_id_fk
FOREIGN KEY (example_solution) REFERENCES public.example_work (id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE public.problem
  ADD CONSTRAINT problem_case_info_id_fk
FOREIGN KEY (case_info_id) REFERENCES public.case_info (id) ON DELETE CASCADE ON UPDATE CASCADE;

/**
Пример работ
 */
ALTER TABLE public.example_work
  ADD CONSTRAINT example_work_specialist_id_fk
FOREIGN KEY (case_info_id) REFERENCES public.specialist (id) ON DELETE CASCADE ON UPDATE CASCADE;

/**
Комплексный кейс **
 */
ALTER TABLE public.example_work
  ADD CONSTRAINT example_work_integrated_case_caseinfo_id_fk
FOREIGN KEY (case_info_id) REFERENCES integrated_case (caseinfo_id) ON DELETE CASCADE ON UPDATE CASCADE;

/**
Таблица кейс
 */
ALTER TABLE public.integrated_case
  ADD CONSTRAINT integrated_case_case_info_id_fk
FOREIGN KEY (caseinfo_id) REFERENCES public.case_info (id) ON DELETE CASCADE ON UPDATE CASCADE;

/**
Таблица кейс
 */
ALTER TABLE public.case_info
  ADD CONSTRAINT case_info_userinfo_id_fk
FOREIGN KEY (userinfo_id) REFERENCES public.userinfo (id) ON DELETE CASCADE ON UPDATE CASCADE;

/**
Таблица кейс
 */
ALTER TABLE public.case_info
  ADD CONSTRAINT case_info_properties_case_id_fk
FOREIGN KEY (id) REFERENCES public.properties_case (id) ON DELETE CASCADE ON UPDATE CASCADE;

/**
Таблица кейс
 */
ALTER TABLE public.case_info
  ADD CONSTRAINT case_info_nomenclature_id_nomenclature_id_fk
FOREIGN KEY (nomenclature_id) REFERENCES public.nomenclature (id) ON DELETE CASCADE ON UPDATE CASCADE;

/**
Таблица номенклатура
 */
ALTER TABLE public.nomenclature
  ADD CONSTRAINT nomenclature_id_properties_id_fk
FOREIGN KEY (id) REFERENCES public.properties_product (id) ON DELETE CASCADE ON UPDATE CASCADE;








