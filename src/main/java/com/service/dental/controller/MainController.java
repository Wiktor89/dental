package com.service.dental.controller;

import com.service.dental.entity.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@RestController
public class MainController {

    @Autowired
    @Qualifier("dataSource")
    private DataSource dataSource;

    @GetMapping(value = "/admin")
    public String hello() {
        return "hello.html";
    }

    @GetMapping(value = "/login")
    public List<UserInfo> login() {
        try {
            Statement statement = dataSource.getConnection ().createStatement ();
            ResultSet resultSet = statement.executeQuery ("SELECT * FROM users");
            if (resultSet != null) {
                return userInfoList (resultSet);
            }
        } catch (SQLException e) {
            e.printStackTrace ();
        }
        return null;
    }

    @GetMapping(value = "/user")
    public String start() {
        return "user.html";
    }

    @GetMapping(value = "/an")
    public String an() {
        return "home.html";
    }

    private List<UserInfo> userInfoList(ResultSet r) throws SQLException {
        List<UserInfo> list = new ArrayList<> ();
        while (r.next ()) {
            UserInfo userInfo = new UserInfo ();
            userInfo.setLogin (r.getString ("username"));
            userInfo.setPassword (r.getString ("password"));
            list.add (userInfo);
        }
        return list;
    }
}
