package com.service.dental.securiti;

import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.stereotype.Component;

@Component
@EnableWebSecurity
public class AuthenticationConfiguration extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication ()
                .withUser ("user")
                .password ("{noop}password")
                .roles ("USER")
                .and ()
                .withUser ("admin")
                .password ("{noop}admin")
                .roles ("USER", "ADMIN");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.
                authorizeRequests ()
                .antMatchers ("/", "/user").hasAnyRole ("USER", "ADMIN")
                .antMatchers ("/", "/admin").hasRole ("ADMIN")
                .antMatchers ("/", "/an").permitAll ()
                .anyRequest ().authenticated ();
        http
                .formLogin ()
                .loginPage ("/login")
                .permitAll ()
                .and ()
                .logout ()
                .permitAll ();
    }
}
